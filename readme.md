# Habitat

Basic Hapi app set up with Typescript

## Getting Started

Clone from Gitlab, install node modules, and start server

```
git clone https://gitlab.com/Kkarich/habitat.git
cd habitat
npm install
npm run start
```

### Coding Style: TS Lint

TS Lint is run in `npm run start` for coding style. Never used this, just thought I would throw it in here


## Updating



## Deployment

Merging to master will auto deploy to Elastic Beanstalk