import { Server } from "hapi";
const port = process.env.PORT || 3000;
const server = new Server({port});

server.route({
    method: "GET",
    path: "/",
    handler: (request, h) => {
        return "Hello, world!";
    },
});

server.route({
    method: "GET",
    path: "/{name}",
    handler: (request, h) => {
        return "Hello, " + encodeURIComponent(request.params.name) + "!";
    },
});

const init = async () => {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

process.on("unhandledRejection", (err) => {
    console.log(err);
    process.exit(1);
});

init();
